# VUE PWA (Progressive Web Application)

## Intro
RND ini bertujuan untuk menghasilkan web aplikasi yang dapat diinstall pada perangkat pengguna (desktop/mobile). Pada RND kali ini menggunakan `vue`, `vite`, `typescript` dan library `vite-plugin-pwa`. Browser yang digunakan adalah `Google Chrome` dan `Mozilla Firefox Developer Edition` dengan extension [`Progressive Web Apps for Firefox`](https://addons.mozilla.org/en-US/firefox/addon/pwas-for-firefox/?utm_content=addons-manager-reviews-link&utm_medium=firefox-browser&utm_source=firefox-browser). Untuk melihat aplikasi yang sudah ada dapat klik https://vue-pwa-fauzanashari-216b59ed78839f5ec03860427242928cf48d82cd8b.gitlab.io/.

## Terminologi 📖
PWA adalah aplikasi berbasis web namun dapat diinstall pada perangkat desktop maupun mobile dengan UX seperti menggunakan aplikasi native. Namun PWA sangat berbeda dengan aplikasi desktop berbasis web sepert `Electron`, `Ionic`, dll.

- *Workbox*: tools yang digunakan untuk melakukan caching assets serta versioning caching.
- *Service worker*: berfungsi sebagai proxy antara browser dan network. Service worker dapat melakukan caching setiap kali koneksi ke network sedang berlangsung. Service worker harus didaftarkan ke browser dan terjadi pada saat pertama kali halaman aplikasi yang terdapat service worker dimuat oleh browser.
- *Manifest*: merupakan meta data dari aplikasi PWA.

## Instalasi ⚙
1. Install package
```bash
   $ npm i -D vite-plugin-pwa@0.17.4  
```

2. Register service worker di `index.html`
```html
<head>
   ....
   <script>
      if('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
          navigator.serviceWorker.register('/sw.js', { scope: '/' })
        })
      }
   </script>
   ....
</head>
```

3. Melakukan generate asset icon dapat melalui tautan [Web App Manifest Generator](https://manifest-gen.netlify.app/) lalu file hasil generate tersebut dapat ditaruh di dalam folder `public/`
```bash
➜ rnd-pwa (feat/vue-pwa) ls public/ -al
total 628
drwxr-xr-x 1 DESKTOP-FIT 197121      0 Jan 16 14:22 ./
drwxr-xr-x 1 DESKTOP-FIT 197121      0 Jan 17 10:29 ../
-rw-r--r-- 1 DESKTOP-FIT 197121  25047 Jan 16 07:18 icon-128x128.png
-rw-r--r-- 1 DESKTOP-FIT 197121  32325 Jan 16 07:18 icon-144x144.png
-rw-r--r-- 1 DESKTOP-FIT 197121  35511 Jan 16 07:18 icon-152x152.png
-rw-r--r-- 1 DESKTOP-FIT 197121  51966 Jan 16 07:18 icon-192x192.png
-rw-r--r-- 1 DESKTOP-FIT 197121 170965 Jan 16 07:18 icon-384x384.png
-rw-r--r-- 1 DESKTOP-FIT 197121 274694 Jan 16 07:18 icon-512x512.png
-rw-r--r-- 1 DESKTOP-FIT 197121   9657 Jan 16 07:18 icon-72x72.png  
-rw-r--r-- 1 DESKTOP-FIT 197121  15941 Jan 16 07:18 icon-96x96.png  
-rw-r--r-- 1 DESKTOP-FIT 197121   1497 Jan 16 10:34 vite.svg     
```

4. Menambahkan manifest ke file `vite.config.ts`, lihat [disini](https://developer.mozilla.org/en-US/docs/Web/Manifest) untuk melihat daftar property manifest yang dapat digunakan
```typescript
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      mode: 'development',
      registerType: 'autoUpdate',
      injectRegister: 'auto',
      workbox: {
        cleanupOutdatedCaches: true,
        globPatterns: ['**/*.{js,css,html,ico,png,svg,json,vue,txt,woff2}']
      },      
      manifest: {
        name: 'Asharifauzan vite pwa app',
        short_name: 'asharifauzan',
        description: 'Currently learning implement pwa in vite app',
        theme_color: '#190323',
        background_color: '#232323',
        display: 'standalone',
        start_url: '/',
        prefer_related_application: true,
        icons: [
          {
            "src": "/icon-72x72.png",
            "sizes": "72x72",
            "type": "image/png"
          },
          {
            "src": "/icon-96x96.png",
            "sizes": "96x96",
            "type": "image/png"
          },
          {
            "src": "/icon-128x128.png",
            "sizes": "128x128",
            "type": "image/png"
          },
          {
            "src": "/icon-144x144.png",
            "sizes": "144x144",
            "type": "image/png",
          },
          {
            "src": "/icon-152x152.png",
            "sizes": "152x152",
            "type": "image/png"
          },
          {
            "src": "/icon-192x192.png",
            "sizes": "192x192",
            "type": "image/png"
          },
          {
            "src": "/icon-384x384.png",
            "sizes": "384x384",
            "type": "image/png"
          },
          {
            "src": "/icon-512x512.png",
            "sizes": "512x512",
            "type": "image/png",
            "purpose": "maskable"
          },
          {
            "src": "/icon-512x512.png",
            "sizes": "512x512",
            "type": "image/png",
            "purpose": "any"
          }
        ]
      }
    })
  ],
})
```

## Build 🏗

5. Build dan jalankan aplikasi
```bash
npm run build && npm run preview -- --host 0.0.0.0
```

## Penggunaan 💻

6. Melihat status service worker apakah sudah terdaftar atau belum
![status service worker](./docs/see-service-worker.png)

7. Melihat manifest
![status service worker](./docs/see-manifest.png)

8. Install aplikasi
![installation](./docs/install-app.png)

9. Aplikasi terbuka
![running app](./docs/installed-pwa.png)

10. Shortcut aplikasi
![shortcur](./docs/search-pwa.png)

## Debugging 🐞

11. Membuka tab *Lighthouse* pada console
![lighthouse](./docs/lighthouse.png)

12. Pastikan pengecekan *PWA* berhasil dan tidak ada error
![lighthouse](./docs/lighthouse-success.png)

## Kelebihan 👍
- Mudah digunakan hanya dengan menginstall satu library saja
- Dokumentasi mudah dimengerti

## Kekurangan 👎
- Harus build aplikasi untuk melihat hasil service worker

## Kesimpulan 🧾
Membangun aplikasi PWA dengan library `vite-plugin-pwa` sangat mudah dan cepat namun kita harus melakukan build aplikasi apabila ada perubahan code. Untuk development disarankan menggunakan browser `Google Chrome` dan menggunakan fitur `Lighthouse`.

## Daftar Referensi 📎
- https://vite-pwa-org.netlify.app/
- https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps
- https://medium.com/the-web-tub/build-a-pwa-using-workbox-2eda1ef51d88
- https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API
